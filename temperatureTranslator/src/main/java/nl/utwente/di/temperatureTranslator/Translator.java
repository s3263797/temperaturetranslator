package nl.utwente.di.temperatureTranslator;

import java.util.HashMap;

public class Translator {

    HashMap<String, Double> temperatures;

    public void hashMapCreator(){
        temperatures = new HashMap<>();
        for (int i = 50; i > -20; i--) {
            double multiplier = 1.8;
            double fahrenheit = (i * (multiplier)) + 32;
            String celsius = Integer.toString(i);
            temperatures.put(celsius, fahrenheit);
        }
    }


    public double getFahrenheit(String celsius){
        hashMapCreator();
        return temperatures.get(celsius);
    }

}
